package com.catalog.repository;

import com.catalog.data.Discount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface DiscountRepository extends JpaRepository<Discount, String> {

    @Query(value = "DELETE FROM discounts WHERE id = :id", nativeQuery = true)
    @Modifying
    @Transactional
    int deleteDiscountById(@Param("id") String id);
}
