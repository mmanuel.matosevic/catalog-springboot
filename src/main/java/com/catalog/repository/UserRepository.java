package com.catalog.repository;

import com.catalog.data.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface UserRepository  extends JpaRepository<User, String> {

    @Query("SELECT username FROM User WHERE username = :username")
    public User getUserByUsername(@Param("username") String username);

}
