package com.catalog.repository;

import com.catalog.data.Article;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface ArticleRepository extends JpaRepository<Article, String> {

    @Query(value = "DELETE FROM articles WHERE id = :id", nativeQuery = true)
    @Modifying
    @Transactional
    int deleteArticleById(@Param("id") String id);

}
