package com.catalog.data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

@Entity
@Table(name="articles")
public class Article implements Serializable {

    @Id
    private String id = UUID.randomUUID().toString();
    private String articleName;
    private Double price;
    @ManyToOne
    private Discount discount;

    public Article() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getArticleName() {
        return articleName;
    }

    public void setArticleName(String articleName) {
        this.articleName = articleName;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Discount getDiscount() {
        return discount;
    }

    public void setDiscount(Discount discount) {
        this.discount = discount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Article)) return false;
        Article article = (Article) o;
        return getId().equals(article.getId()) &&
                getArticleName().equals(article.getArticleName()) &&
                getPrice().equals(article.getPrice()) &&
                Objects.equals(getDiscount(), article.getDiscount());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getArticleName(), getPrice(), getDiscount());
    }

    @Override
    public String toString() {
        return "Article{" +
                "id='" + id + '\'' +
                ", articleName='" + articleName + '\'' +
                ", price=" + price +
                ", discount=" + discount +
                '}';
    }
}
