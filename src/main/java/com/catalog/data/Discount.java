package com.catalog.data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Entity
@Table(name="discounts")
public class Discount implements Serializable {

    @Id
    private String id = UUID.randomUUID().toString();
    private String startDate;
    private String endDate;
    private Integer percentage;
    @OneToMany
    private List<Article> articles;

    public Discount() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public Integer getPercentage() {
        return percentage;
    }

    public void setPercentage(Integer percentage) {
        this.percentage = percentage;
    }

    public List<Article> getArticles() {
        return articles;
    }

    public void setArticles(List<Article> articles) {
        this.articles = articles;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Discount)) return false;
        Discount discount = (Discount) o;
        return getId().equals(discount.getId()) &&
                getStartDate().equals(discount.getStartDate()) &&
                getEndDate().equals(discount.getEndDate()) &&
                getPercentage().equals(discount.getPercentage()) &&
                Objects.equals(getArticles(), discount.getArticles());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getStartDate(), getEndDate(), getPercentage(), getArticles());
    }

    @Override
    public String toString() {
        return "Discount{" +
                "id='" + id + '\'' +
                ", startDate='" + startDate + '\'' +
                ", endDate='" + endDate + '\'' +
                ", percentage=" + percentage +
                ", articles=" + articles +
                '}';
    }
}
