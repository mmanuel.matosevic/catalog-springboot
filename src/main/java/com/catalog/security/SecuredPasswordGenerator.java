package com.catalog.security;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

//GENERATE IN-MEMORY PASSWORD FOR WebSecurityConfig/userDetailsService

public class SecuredPasswordGenerator {

    public static void main(String[] args){
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        String rawPassword = "sales";
        String encodedPassword = encoder.encode(rawPassword);

        System.out.println(encodedPassword);
    }
}
