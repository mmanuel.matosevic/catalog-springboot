package com.catalog.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Bean
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }

    @Override
    @Bean
    protected UserDetailsService userDetailsService() {
        UserDetails admin = User.withUsername("admin")
                .password("$2a$10$Qx/zacKnNe4a5wq1iDrhROC7is0vUXQJ2dUVoxzVEExU/rg/NEqYG")
                .roles("ADMIN")
                .build();

        UserDetails customer = User.withUsername("customer")
                .password("$2a$10$EZiXVloq3XfoJWIUApqwiOx3MKk1UHRY0gwjkdXD3g6DjH23/VT7S")
                .roles("CUSTOMER")
                .build();

        UserDetails sales = User.withUsername("sales")
                .password("$2a$10$V9Phg2YwIN0bLUsJYMV7vOKqBxRN0xF3khTJNuxQ3OOVi.6a98xg2")
                .roles("SALES")
                .build();


        UserDetails factory = User.withUsername("factory")
                .password("$2a$10$rCOxIrBIdbbbbHn2NsrREu.kPc.eNqI/oCPfEvkjCkk3XGsO48SY6")
                .roles("FACTORY")
                .build();

        InMemoryUserDetailsManager inMemoryUserDetailsManager = new InMemoryUserDetailsManager();
        inMemoryUserDetailsManager.createUser(admin);
        inMemoryUserDetailsManager.createUser(customer);
        inMemoryUserDetailsManager.createUser(sales);
        inMemoryUserDetailsManager.createUser(factory);

        return inMemoryUserDetailsManager;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers("/login").permitAll()
                .mvcMatchers("/customer").hasRole("CUSTOMER")
                .mvcMatchers("/home").hasRole("SALES")
                .anyRequest().authenticated()
                .and()
                .formLogin()
                    .loginPage("/login")
                    .defaultSuccessUrl("/redirect", true)
                    .permitAll()
                .and()
                .logout().permitAll();
    }



}
