package com.catalog.controllers;

import com.catalog.repository.ArticleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class FactoryController {

    @Autowired
    ArticleRepository articleRepository;

    @GetMapping("/factory")
    public String sales(Model model){
        model.addAttribute("articles", articleRepository.findAll());
        return "layout/factory";
    }
}
