package com.catalog.controllers;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.view.RedirectView;



@Controller
public class UsersController {

    @GetMapping("/redirect")
    public RedirectView roles(Authentication auth){
        RedirectView redirectView = new RedirectView();
        String role = auth.getAuthorities().toString();
        System.out.println(role);
        if(role.equals("[ROLE_ADMIN]")){
            System.out.println("admin");
            redirectView.setUrl("http://localhost:8080/admin");
        }
        else if(role.equals("[ROLE_CUSTOMER]")){
            System.out.println("customer");
            redirectView.setUrl("http://localhost:8080/customer");
        }
        else if(role.equals("[ROLE_SALES]")){
            System.out.println("sales");
            redirectView.setUrl("http://localhost:8080/sales");
        }
        else if(role.equals("[ROLE_FACTORY]")){
            System.out.println("factory");
            redirectView.setUrl("http://localhost:8080/factory");
        }
        else{
            redirectView.setUrl("http://localhost:8080/error");
        }
        return redirectView;
    }


}
