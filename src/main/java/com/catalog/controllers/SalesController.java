package com.catalog.controllers;

import com.catalog.data.Article;
import com.catalog.data.Discount;
import com.catalog.repository.ArticleRepository;
import com.catalog.repository.DiscountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class SalesController {

    @Autowired
    ArticleRepository articleRepository;
    @Autowired
    DiscountRepository discountRepository;

    @GetMapping("/sales")
    public String sales(Model model){
        model.addAttribute("articles", articleRepository.findAll());
        model.addAttribute("discounts", discountRepository.findAll());
        return "layout/sales";
    }

    @GetMapping("/sales/addArticle")
    public String addArticle(Model model){
        model.addAttribute("article",new Article());
        return "layout/addArticle.html";
    }

    @PostMapping("/sales/addArticle")
    public String processAddArticle(@ModelAttribute(value="article") Article article){
        articleRepository.save(article);
        return "redirect:/sales";
    }

    @GetMapping("/sales/addDiscount")
    public String addDiscount(Model model){
        model.addAttribute("discount",new Discount());
        return "layout/addDiscount";
    }

    @PostMapping("/sales/addDiscount")
    public String processAddDiscount(@ModelAttribute(value="discount") Discount discount){
        discountRepository.save(discount);
        return "redirect:/sales";
    }

    @RequestMapping(value = "/sales/deleteArticle", method = RequestMethod.GET)
    public String processDeleteArticle(@RequestParam(name="articleId")String articleId) {
        articleRepository.deleteArticleById(articleId);
        return "redirect:/sales";
    }

    @RequestMapping(value = "/sales/deleteDiscount", method = RequestMethod.GET)
    public String processDeleteDiscount(@RequestParam(name="discountId")String discountId) {
        discountRepository.deleteDiscountById(discountId);
        return "redirect:/sales";
    }


}
